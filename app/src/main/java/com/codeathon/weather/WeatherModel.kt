package com.codeathon.weather

/**
 * @author Ahmad H. Ibrahim on 4/15/19.
 */
data class WeatherModel(val temperature: Double, val summary: String, val icon: String,
                        val latitude: Double, val longitude: Double)


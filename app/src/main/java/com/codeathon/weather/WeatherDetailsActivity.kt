package com.codeathon.weather

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProviders
import com.codeathon.weather.databinding.WeatherDetailsBinding
import kotlinx.android.synthetic.main.weather_details.*

class WeatherDetailsActivity : AppCompatActivity() {

    private lateinit var viewModel: WeatherViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        // connect viewmodel
        viewModel = ViewModelProviders.of(this).get(WeatherViewModel::class.java)
        // setup databinding
        val mBinding: WeatherDetailsBinding = DataBindingUtil.setContentView(this, R.layout.weather_details)
        mBinding.viewModel = viewModel

        setSupportActionBar(toolbar)
    }
}
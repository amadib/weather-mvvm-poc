package com.codeathon.weather

import android.app.Application
import android.location.Geocoder
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import java.util.*

/**
 * @author Ahmad H. Ibrahim on 4/15/19.
 */
class WeatherViewModel(application: Application) : AndroidViewModel(application) {

    val temp = MutableLiveData<String>()
    val summary = MutableLiveData<String>()
    val icon = MutableLiveData<String>()
    val location = MutableLiveData<String>()

    init {
        load(Repo().loadCurrentWeather())
    }

    private fun load(currentWeather: WeatherModel) {
        temp.value = currentWeather.temperature.toString()
        summary.value = currentWeather.summary
//        icon.value = currentWeather.icon
        icon.value = "ic_wi_rain"
        location.value = getLocation(currentWeather.latitude, currentWeather.longitude)
    }

    fun getLocation(latitude: Double, longitude: Double): String {
        val geocoder = Geocoder(getApplication(), Locale.getDefault())
        val addresses = geocoder.getFromLocation(latitude, longitude, 1)
        val cityName = addresses[0].locality
        val stateName = addresses[0].adminArea
        val stateCode = StateUtils.getState(stateName)
        return "$cityName, $stateCode"
    }
}
